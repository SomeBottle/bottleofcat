# bottleofcat
这里是Somebottle的摸鱼学习笔记   
肯定会有错误，欢迎各位指教(*‘ v\`*)   
咱在这儿的笔记也是和[咱的博客](https://bottle.moe)相同步的~   

-------
Here are the learning notes written by an amateur bottle.  
There must be something that I misunderstood , I'll appreciate it if you can point them out~  
The notes here will be synchronized with my blog.  

-------
## Content  
0. [书签签](bookmarks.md)  
1. [JS小记：闭...闭包？](JavaScript/JavaScript-Closure.md)  
2. [大学物理实验有效数字与测量值小记](Physics/PhyExpMeasuredAndSignificantFigure.md)  
3. [【动画解释】关系数据库de关系代数小记](Database/DatabaseRelationalAlgebra.md) <--流量用户慎重浏览   
4. [Python里字符串Format时的一个易错“点”](Python/DontForgetDotInFormat.md)  
5. [Python正则表达式细节小记](Python/TipsOfRegex.md)  
6. [JavaScript正则表达式replace的一个细节点](JavaScript/watchOutRegexInReplace.md)  
7. [Python面向对象小备忘](Python/NoteOfPythonOOP.md)  
8. [【动画笔记】二分查找（折半查找）](Algo/BinarySearch.md)  
9. [物理实验霍尔效应判断P/N型半导体笔记](Physics/HallEfxAndSemiconductor.md)  

## Experimental  
0. [Python简单多方法实现扑克牌随机分发](Python/SimplePokerDistribution/poker.py)  
1. [Python一排洞抓狐狸小游戏](Python/WhereDoesTheFoxHide/fox.py)  
2. [Python实验之简单的命令行学生管理](Python/ExpStuManagement/stu.py)  
3. [Python课设-SKLINE](https://github.com/SomeBottle/skline)  

在拆那就要说拆那话啊喂！ヽ(ｏ\`皿′ｏ)ﾉ   

![](https://ae01.alicdn.com/kf/U74e1f2db572e49fb829c11596f3a6233T.jpg)  
